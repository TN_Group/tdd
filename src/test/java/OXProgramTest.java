/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HP
 */
public class OXProgramTest {
    
    public OXProgramTest() {
    }

     @Test
    public void testCheckVerticalPlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, 
                          {'X', '-', '-'}, 
                          {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true,OX_Game.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, 
                          {'-', 'X', '-'}, 
                          {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true,OX_Game.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, 
                          {'-', '-', 'X'}, 
                          {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true,OX_Game.checkVertical(table, currentPlayer, col));
    }

   @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                          {'O', '-', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true,OX_Game.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                          {'-', 'O', '-'}, 
                          {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true,OX_Game.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', '-', 'O'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true,OX_Game.checkVertical(table, currentPlayer, col));
    }

     @Test
    public void testCheckHorizontalPlayerXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                          {'-', '-', '-'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        assertEquals(true,OX_Game.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerXRow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'X', 'X', 'X'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;
        assertEquals(true,OX_Game.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerXRow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'}, 
                          {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;
        assertEquals(true,OX_Game.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                          {'-', '-', '-'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true,OX_Game.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'O', 'O', 'O'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true,OX_Game.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'}, 
                          {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true,OX_Game.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckX1PlayerXWin() {
        char table[][] = {{'X', '-', '-'}, 
                          {'-', 'X', '-'}, 
                          {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true,OX_Game.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerXWin() {
        char table[][] = {{'-', '-', 'X'}, 
                          {'-', 'X', '-'}, 
                          {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true,OX_Game.checkX2(table, currentPlayer));
    }

    @Test
    public void testCheckX1PlayerOWin() {
        char table[][] = {{'O', '-', '-'}, 
                          {'-', 'O', '-'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true,OX_Game.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerOWin() {
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', 'O', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true,OX_Game.checkX2(table, currentPlayer));
    }



}
